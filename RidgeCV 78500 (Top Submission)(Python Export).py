#!/usr/bin/env python
# coding: utf-8

# In[1]:


#importing libraries
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import statsmodels.api as sm
get_ipython().run_line_magic('matplotlib', 'inline')
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.feature_selection import RFE
from sklearn.linear_model import RidgeCV, LassoCV, Ridge, Lasso
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder, LabelEncoder, KBinsDiscretizer
from math import sqrt

# Read in combined dataset of both the labled and unlabled data
# Combined set is used so that the preprocessing done is the same for the labled dataset and the unlabled dataset that we want to predict on
combined = pd.read_csv("CombineDdata.csv")


# In[2]:


#Split data into income set (Incuding instance so that the unlabled data can extracted later)
y = combined[["Instance","Income in EUR"]]

#And all the Feature data (Incuding instance so that the unlabled data can extracted later)
X = combined[["Instance","Year of Record","Age","Profession","Gender","Country","University Degree","Size of City","Body Height [cm]","Wears Glasses","Hair Color"]].copy()


# In[3]:


#Function to relpace invalid entries in numeric features with the mean walue of the particular feature
def ReplaceNan_Numeric(df,F_Name):
    average = df[F_Name].dropna().mean(axis=0)
    df[F_Name] = df[F_Name].replace(np.nan, average, inplace=False)
    return df


# In[4]:


# Function to onehot encode catagorical features
def oneHotEncode_Feature(df,F_Name):
    feature = df[F_Name]
    label_encoder = LabelEncoder()
    integer_encoded = label_encoder.fit_transform(feature)
    
    #binary encode
    onehot_encoder = OneHotEncoder(sparse=False)
    integer_encoded = integer_encoded.reshape(len(integer_encoded), 1)
    onehot_encoded = onehot_encoder.fit_transform(integer_encoded)

    feature_onehot = pd.DataFrame(onehot_encoded)
    df = df.drop(F_Name,1)   #Feature Matrix
    new_df = pd.concat([df, feature_onehot], axis=1)
    return new_df


# In[5]:


# Function to convert non-linear numeric data into 'bins' of nearby values, bins represented with onehot encoding
def KBins_Feature(df, F_Name):
    feature_array = df[F_Name].to_numpy()
    feature_array = feature_array.reshape(-1, 1)
    est = KBinsDiscretizer(n_bins=8, encode='onehot-dense', strategy='quantile')
    est.fit(feature_array)
    onehot_encoded = est.transform(feature_array)
    
    onehot_feature_df = pd.DataFrame(onehot_encoded)
    df = df.drop(F_Name,1)   #Feature Matrix
    new_df = pd.concat([df, onehot_feature_df], axis=1)
    return new_df


# In[6]:



def MinMaxScale_Feature(df, F_Name):
    feature_array = df[F_Name].to_numpy()
    feature_array = feature_array.reshape(-1, 1)
    
    scaler = MinMaxScaler()
    scaled_array = scaler.fit_transform(feature_array)
    
    scaled_df = pd.DataFrame(scaled_array)
    df = df.drop(F_Name,1)   #Feature Matrix
    new_df = pd.concat([df, scaled_df], axis=1)
    return new_df


# In[7]:


# Clean numeric data
X = ReplaceNan_Numeric(X,"Year of Record")
X = ReplaceNan_Numeric(X,"Body Height [cm]")
X = ReplaceNan_Numeric(X,"Age")
X = ReplaceNan_Numeric(X,"Size of City")


# In[8]:


# Scale numeric data
X = MinMaxScale_Feature(X,"Year of Record")
X = MinMaxScale_Feature(X,"Age")
X = MinMaxScale_Feature(X,"Body Height [cm]")


# In[9]:


# Split Size of cities into bins 
X = KBins_Feature(X,"Size of City")


# In[10]:

# Clean and onehot encode categorical features
X["Profession"] = X["Profession"].replace(np.nan, "Unknown", inplace=False)
X = oneHotEncode_Feature(X,"Profession")

X["Gender"] = X["Gender"].replace(np.nan, "unknown", inplace=False)
X = oneHotEncode_Feature(X,"Gender")

X["Country"] =  X["Country"].replace(np.nan, "Unknown", inplace=False)
X = oneHotEncode_Feature(X,"Country")

X["University Degree"] = X["University Degree"].replace(np.nan, "Unknown", inplace=False)
X = oneHotEncode_Feature(X,"University Degree")

X["Hair Color"] = X["Hair Color"].replace(np.nan, "Unknown", inplace=False)
X = oneHotEncode_Feature(X,"Hair Color")


# In[11]:

#Split the data back out to separate sets on key 'Instance'
df2_X = X.loc[X["Instance"] > 111993]
X = X.loc[X["Instance"] <= 111993]         
#df2_y = y.loc[y["Instance"] > 111993] #not needed
y = y.loc[y["Instance"] <= 111993]

#Drop 'Instance' from datasets not that it is nolonger needed as a key
df2_X = df2_X.drop("Instance",1)
X = X.drop("Instance",1)
y = y.drop("Instance",1)


# In[14]:


# Split the test and training data
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)

# Create ridgecv regression object with 4 crossvalidation folds
regr = RidgeCV(alphas=np.array([0.0001,0.001,0.01,0.1,1]),fit_intercept=True,normalize=False,cv = 4)

# Train the model using the training sets
regr.fit(X_train, y_train)

# Make predictions using the testing set
y_pred = regr.predict(X_test)

# Print results
print("Root Mean squared error: %.2f"
      % sqrt(mean_squared_error(y_test, y_pred)))

# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f' % r2_score(y_test, y_pred))


# In[13]:


# Make predictions on the unlabled dataset
df2_y_pred = regr.predict(df2_X)
# export data
pd.DataFrame(df2_y_pred).to_csv("Predictions.csv")

